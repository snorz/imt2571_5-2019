<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();

        $actorsN = '//Actors/Actor/Name';

        $actorName = $this->xpath->query($actorsN); // gets actorsNames

        foreach($actorName as $node){    // Looping through each actor
            $act = $node->nodeValue;     // actor name
            $result[$act] = array();     // make a new array inside

            $actorID = "//Actors/Actor[Name = '$act']/@id";
            $actID   = $this->xpath->query($actorID);    // get actor ID
            $actID   = $actID->item(0)->value;           // gets value

            $movieN  = "//Subsidiaries/Subsidiary/Movie[Cast/Role
                        [@actor ='$actID']]/Name";
            $movLi   = $this->xpath->query($movieN); // retrieve movies of actors

            foreach($movLi as $movie){              // loop through each movie
                $movieVal = $movie->nodeValue;      // get's value of movie-node

                $nameInMovie = "//Subsidiaries/Subsidiary/Movie[Name='$movieVal']
                                /Cast/Role[@actor = '$actID']/@name";
                $aMovName = $this->xpath->query($nameInMovie); // find role name
                $aMovNameRes = $aMovName->item(0);
                $aMovNameRes = $aMovNameRes->nodeValue; // gets value
                                                            // year of movie
                $movYear = $movie->nextSibling->nextSibling->nodeValue;

                                // making the string that will be stored
                $inString = "As " . $aMovNameRes . " in " . $movieVal . " (" . $movYear . ")";
                                        // passing array of each actor to result
                array_push($result[$act],$inString);
            }
        }

        return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
                                        // path to find all Unreferenced Actors
        $remove = "//Actors/Actor[not (@id = //Subsidiaries/Subsidiary/Movie/Cast/Role/@actor)]";

        $remLi = $this->xpath->query($remove);    // runs query
        foreach($remLi as $node){                 // loops through all returned
            $node->parentNode->removeChild($node);//deletes nodes
        }

    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {

        $role = $this->doc->createElement('Role'); // creates a new role
        $roleEl = $this->xpath->query("Subsidiaries/Subsidiary
                                      [@id = '$subsidiaryId']/Movie[Name='$movieName'
                                      and Year='$movieYear']/Cast")[0];
        $roleEl->appendChild($role);                // adds a new child

        $role->setAttribute('name', $roleName);     // assigns attributes:
        $role->setAttribute('actor', $roleActor);

        if($roleAlias > 0){                             // if alias != 0
            $role->setAttribute('alias', $roleAlias);   //    set alias
        }
    }
}
?>
